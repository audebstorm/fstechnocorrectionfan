import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FanRoutingModule } from './fan-routing.module';
import { FanComponent } from './fan.component';
import { BrowserModule } from '@angular/platform-browser';
import { FanDetailsComponent } from './fan-details/fan-details.component';
import { FanService } from './services/fan.service';
import { FanCreateComponent } from './fan-create/fan-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FanEditComponent } from './fan-edit/fan-edit.component'


@NgModule({
  declarations: [
    FanDetailsComponent,
    FanCreateComponent,
    FanEditComponent
  ],
  imports: [
    CommonModule,
    FanRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    FanService
  ],
  bootstrap: [FanComponent]
})
export class FanModule { }
