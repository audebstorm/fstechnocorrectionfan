import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FanCreateComponent } from './fan-create/fan-create.component';
import { FanDetailsComponent } from './fan-details/fan-details.component';
import { FanEditComponent } from './fan-edit/fan-edit.component';
import { FanComponent } from './fan.component';

const routes: Routes = [
  { path : '', component : FanComponent},
  { path : 'details/:id', component : FanDetailsComponent},
  { path : 'create', component : FanCreateComponent},
  { path : 'edit/:id', component : FanEditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FanRoutingModule { }
