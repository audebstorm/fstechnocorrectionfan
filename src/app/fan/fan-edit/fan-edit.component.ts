import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { peggi13Validator } from '../customs/validators/peggi13';
import { Serie, Fan } from '../models/Fan';
import { FanService } from '../services/fan.service';

@Component({
  selector: 'app-fan-edit',
  templateUrl: './fan-edit.component.html',
  styleUrls: ['./fan-edit.component.scss']
})
export class FanEditComponent implements OnInit {
  fanForm : FormGroup;
  fanId : number;

  constructor(private _fb : FormBuilder, 
             private _fanService : FanService,
             private _router : Router,
             private _aRouter : ActivatedRoute) {

    //Récupération id
    this.fanId = parseInt(this._aRouter.snapshot.params['id']);

    //Création formulaire
    this.fanForm = this._fb.group({
      name : ['', [Validators.required]],
      birthdate : ['', [Validators.required, peggi13Validator()]],
      series : this._fb.array([
        //this._fb.control('', [Validators.required]),
      ])

    })
  }

  ngOnInit(): void { 
      let fan : Fan | undefined = this._fanService.getById(this.fanId);
      if(fan) {
        //Mise à jour formulaire
        console.log( `${fan.birthdate.getFullYear()}-${(fan.birthdate.getMonth()+1 < 10 ) ? '0'+fan.birthdate.getMonth()+1 : fan.birthdate.getMonth()+1 }-${(fan.birthdate.getDate() < 10 ) ? '0'+fan.birthdate.getDate() : fan.birthdate.getDate()}`);
        
        //On modifie le formulaire (de base sans séries) avec les données du fan récupéré
        this.fanForm.patchValue({
          name : fan.name,
          birthdate : `${fan.birthdate.getFullYear()}-${(fan.birthdate.getMonth()+1 < 10 ) ? '0'+(fan.birthdate.getMonth()+1) : (fan.birthdate.getMonth()+1) }-${(fan.birthdate.getDate() < 10 ) ? '0'+fan.birthdate.getDate() : fan.birthdate.getDate()}`
        })
        //On créer autant de "formulaires" série que le fan a de séries
        fan.listSeries.forEach(serie => {
          this.addSeries();          
        });
        //On les met à jour
        //this.series.patchValue(["Dr Who", "Fringe"])
        //[{ id : 1, title : "Dr Who"}, {id : 2, title : "Fringe"}]
        //["Dr Who", "Fringe"]
        this.series.patchValue(fan.listSeries.map(s => s.title));
      }
      else {
        //Id pas trouvé -> redirection
        //Idéalement une page 404
        //this._router.navigateByUrl('/notfound');
      }
  }

  get series() {
    return this.fanForm.get('series') as FormArray;
  }

  addSeries() {
    this.series.push(this._fb.control('', [Validators.required]))
  }

  removeSerie(i : number) {
    this.series.removeAt(i);
  }

  updateFan() {
    if(this.fanForm.valid) {
      //console.log(this.fanForm.value);
      let seriesToAdd : Serie[] = []
      for(let i in this.fanForm.value.series) {
        seriesToAdd.push({ id : parseInt(i)+1, title : this.fanForm.value.series[i] })        
      }
      let fanToAdd = new Fan(this.fanId, this.fanForm.value.name, new Date(this.fanForm.value.birthdate), seriesToAdd)
      console.log(fanToAdd);
      this._fanService.update(fanToAdd, this.fanId);
      this._router.navigateByUrl('/fan');
      
      
    }
    else {
      this.fanForm.markAllAsTouched();
      //Marque tous les controls du formgroup comme touched
    }
  }

}
