import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FanEditComponent } from './fan-edit.component';

describe('FanEditComponent', () => {
  let component: FanEditComponent;
  let fixture: ComponentFixture<FanEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FanEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FanEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
