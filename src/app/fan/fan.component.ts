import { Component, OnInit } from '@angular/core';
import { Fan } from './models/Fan';
import { FanService } from './services/fan.service';

@Component({
  selector: 'app-fan',
  templateUrl: './fan.component.html',
  styleUrls: ['./fan.component.scss']
})
export class FanComponent implements OnInit {
  listFan : Fan[] = []

  constructor(private _fanService : FanService) {

  }

  ngOnInit(): void {
      this.listFan = this._fanService.getAll();
  }

  delete(id: number) : void {
    this._fanService.delete(id);
  }
}
