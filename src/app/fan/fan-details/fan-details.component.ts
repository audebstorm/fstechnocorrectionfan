import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Fan } from '../models/Fan';
import { FanService } from '../services/fan.service';

@Component({
  selector: 'app-fan-details',
  templateUrl: './fan-details.component.html',
  styleUrls: ['./fan-details.component.scss']
})
export class FanDetailsComponent {

  fan : Fan | undefined ;
  currendId : number;
  
  constructor(private _aRouter : ActivatedRoute, private _fanService : FanService, private _router : Router) {
    this.currendId = parseInt(this._aRouter.snapshot.params["id"]);
    this.fan = this._fanService.getById(this.currendId);
  }

  delete() : void {
    this._fanService.delete(this.currendId);
    this._router.navigateByUrl('/fan');
  }
}
