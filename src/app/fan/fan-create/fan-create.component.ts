import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { peggi13Validator } from '../customs/validators/peggi13';
import { Fan, Serie } from '../models/Fan';
import { FanService } from '../services/fan.service';

@Component({
  selector: 'app-fan-create',
  templateUrl: './fan-create.component.html',
  styleUrls: ['./fan-create.component.scss']
})
export class FanCreateComponent {

  fanForm : FormGroup;


  constructor(private _fb : FormBuilder, private _fanService : FanService, private _router : Router) {
    this.fanForm = this._fb.group({
      name : ['', [Validators.required]],
      birthdate : ['', [Validators.required, peggi13Validator()]],
      series : this._fb.array([
        this._fb.control('', [Validators.required]),
      ])

    })
  }

  get series() {
    return this.fanForm.get('series') as FormArray;
  }

  addSeries() {
    this.series.push(this._fb.control('', [Validators.required]))
  }

  removeSerie(i : number) {
    this.series.removeAt(i);
  }

  addFan() {
    if(this.fanForm.valid) {
      //console.log(this.fanForm.value);
      let seriesToAdd : Serie[] = []
      for(let i in this.fanForm.value.series) {
        seriesToAdd.push({ id : parseInt(i)+1, title : this.fanForm.value.series[i] })        
      }
      let fanToAdd = new Fan(0, this.fanForm.value.name, new Date(this.fanForm.value.birthdate), seriesToAdd)
      console.log(fanToAdd);
      this._fanService.create(fanToAdd);
      this._router.navigateByUrl('/fan');
      
      
    }
    else {
      this.fanForm.markAllAsTouched();
      //Marque tous les controls du formgroup comme touched
    }
  }




}
