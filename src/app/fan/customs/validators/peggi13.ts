import { AbstractControl, ValidatorFn } from "@angular/forms";

export function peggi13Validator() : ValidatorFn | null {
    return (control : AbstractControl) => {
        if(control.value !== '') {
            //On récupère l'année de la date de naissance
            let anneeBD = new Date(control.value).getFullYear();
            //On crée la date du jour et on récup l'année
            let anneeToday = new Date().getFullYear();
            if(anneeToday - anneeBD < 13){
                return { peggi : true }
            }
        }
        return null;
    }
}