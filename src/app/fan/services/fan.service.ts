import { Injectable } from '@angular/core';
import { Fan } from '../models/Fan';

@Injectable(/*{
   providedIn: 'root'
}*/)
export class FanService {

  private fanList : Fan[] = [
    { id : 1, name : 'Aude', birthdate : new Date(1989, 9, 16), listSeries : [
      { id : 1, title : 'Dr Who'},
      { id : 2, title : 'Fringe'}
    ]}
  ]

  constructor() { }

  //R (Read)
  getAll() : Fan[] {
    return this.fanList;
  }

  getById(id : number) : Fan | undefined {
    //Possiblement, si l'id n'est pas présent dans le tableau, la méthode find, va renvoyer undefined
    return this.fanList.find(f => f.id === id);
  }

  //C (Create)
  create(fanToAdd : Fan) : void {
    //On récupère l'id max déjà présent dans notre liste
    let maxIdInList = Math.max(...this.fanList.map(f => f.id)) //[1, 2, 5, 8]
    //On ajoute 1 pour avoir notre nouvel id
    let newId = maxIdInList + 1;
    //On modifie l'id de notre nouveau fan à ajouter
    fanToAdd.id = newId;
    this.fanList.push(fanToAdd);
  }

  //U (Update)
  //update(fanToUpdate : Fan) {} //Isoké aussi, puisqu'on a déjà l'id dans fanToUpdate
  update(fanToUpdate : Fan, idToUpdate : number) : void {
    //Option1 -> On trouve l'index du fan, on le supprime dans la liste, on rajoute le fan modifié
    // let index = this.fanList.findIndex(f => f.id === idToUpdate)
    // this.fanList.splice(index, 1);
    // Ou
    // this.delete(idToUpdate)
    // this.fanList.push(fanToUpdate);

    //Option2 -> on transforme le tableau déjà existant
    
    this.fanList = this.fanList.map(f => {
      //Si id correspond, on remplace par fanToUpdate, sinon, on garde l'ancien fan
      return (f.id === idToUpdate) ? {...fanToUpdate, id : idToUpdate} : f
    });

  }
  
  //D (Delete)
  delete(id : number) : void {
    let index = this.fanList.findIndex(f => f.id === id)
    this.fanList.splice(index, 1);
  }


}
