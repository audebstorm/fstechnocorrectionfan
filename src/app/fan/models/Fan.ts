export class Fan {
    id : number;
    name : string;
    birthdate : Date;
    listSeries : Serie[];

    constructor(id : number, name : string, birthdate : Date, listSeries : Serie[]) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.listSeries = listSeries;
    }
}

export class Serie {
    id : number;
    title : string;

    constructor(id : number, title : string) {
        this.id = id;
        this.title = title;
    }
}




